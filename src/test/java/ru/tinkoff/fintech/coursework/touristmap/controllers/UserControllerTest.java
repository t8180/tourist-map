package ru.tinkoff.fintech.coursework.touristmap.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.coursework.touristmap.DTO.UserDTO;
import ru.tinkoff.fintech.coursework.touristmap.TouristMapApplicationTests;
import ru.tinkoff.fintech.coursework.touristmap.models.User;
import ru.tinkoff.fintech.coursework.touristmap.models.enums.RoleEnum;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "100", roles = "ADMIN")
class UserControllerTest extends TouristMapApplicationTests {
    private static final String START_URL = "/users/";
    private static final String SELECT_USER_BY_NAME_SQL = "select id, username, password, user_role from users where username=";
    private static final String USER_ALREADY_EXISTS = "User already exists with name: ";
    private static final String EXISTS_USER_BY_ID_SQL = "select exists(select * from users where id= %d)";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void isOkFindUserByIDTest() throws Exception {
        long id = 1L;
        UserDTO userDTO = new UserDTO(id, "admin", RoleEnum.ROLE_ADMIN);
        String expectedContent = objectMapper.writeValueAsString(userDTO);
        mockMvc.perform(get(START_URL + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isBusyNameRegistrationTest() throws Exception {
        String username = "log";
        User user = new User(null, username, "100", RoleEnum.ROLE_USER);
        String request = objectMapper.writeValueAsString(user);
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(USER_ALREADY_EXISTS + username));
    }

    @Test
    @Transactional
    void isOkRegistrationTest() throws Exception {
        String name = "log1";
        String password = "100";
        User user = new User(null, name, password, RoleEnum.ROLE_USER);
        String expectedContent = "true";
        String userRequest = objectMapper.writeValueAsString(user);
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(userRequest))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        User actualUser = jdbcTemplate.queryForObject(SELECT_USER_BY_NAME_SQL + "'log1'", getRowMapperUser());
        User expectUser = new User(3L, name, password, RoleEnum.ROLE_USER);
        Assertions.assertEquals(expectUser, actualUser);
    }

    @Test
    @Transactional
    void isOkUpdateTest() throws Exception {
        long id = 2L;
        String name = "log1";
        User user = new User(id, name, "100", RoleEnum.ROLE_USER);
        String request = objectMapper.writeValueAsString(user);
        mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
        User actualUser = jdbcTemplate.queryForObject(SELECT_USER_BY_NAME_SQL + "'log1'", getRowMapperUser());
        assert actualUser != null;
        Assertions.assertEquals(name, actualUser.getUsername());
    }

    @Test
    @Transactional
    void isOkDeleteTest() throws Exception {
        long id = 2L;
        String expectedContent = "true";
        mockMvc.perform(delete(START_URL + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        Boolean resultDelete = jdbcTemplate.queryForObject(String.format(EXISTS_USER_BY_ID_SQL, id), Boolean.class);
        Assertions.assertEquals(Boolean.FALSE, resultDelete);
    }


    @Test
    void isValidUserCreateModelTest() throws Exception {
        User user = new User(3L, "", "", null);
        List<String> errorMessageExpected = List.of(
                "Id must be null",
                "Name can not be blank",
                "Password can not be blank",
                "Role can not be null"
        );
        String request = objectMapper.writeValueAsString(user);
        MvcResult result = mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
        String responseContent = result.getResponse().getContentAsString();
        List<String> responseMessages = List.of(objectMapper.readValue(responseContent, String[].class));
        Assertions.assertTrue(responseMessages.containsAll(errorMessageExpected));
    }

    @Test
    void isValidUserUpdateModelTest() throws Exception {
        User user = new User(null, "", "", null);
        List<String> errorMessageExpected = List.of(
                "Id must be not null",
                "Name can not be blank",
                "Password can not be blank",
                "Role can not be null"
        );
        String request = objectMapper.writeValueAsString(user);
        MvcResult result = mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
        String responseContent = result.getResponse().getContentAsString();
        List<String> responseMessages = List.of(objectMapper.readValue(responseContent, String[].class));
        Assertions.assertTrue(responseMessages.containsAll(errorMessageExpected));
    }


    private RowMapper<User> getRowMapperUser() {
        return (rs, row) -> new User(
                rs.getLong("id"),
                rs.getString("userName"),
                rs.getString("password"),
                RoleEnum.valueOf(rs.getString("user_role"))
        );
    }
}

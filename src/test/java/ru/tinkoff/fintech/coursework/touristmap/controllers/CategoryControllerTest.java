package ru.tinkoff.fintech.coursework.touristmap.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.coursework.touristmap.TouristMapApplicationTests;
import ru.tinkoff.fintech.coursework.touristmap.models.Category;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "100", roles = "ADMIN")
class CategoryControllerTest extends TouristMapApplicationTests {
    private static final String START_URL = "/category/";
    private static final String CATEGORY_NOT_FOUND = "Category not found with id: ";
    private static final String CATEGORY_PARENT_NOT_FOUND_WITH_ID = "Category parent not found with id: ";
    private static final String CATEGORY_HAVE_CHILD = " Or category have child";
    private static final String CATEGORY_ALREADY_CREATED = "Category is already created with name: ";
    public static final String SELECT_CATEGORY_BY_NAME_SQL = "select id, name_category, parent_id from category where name_category=";
    public static final String SELECT_CATEGORY_BY_ID_SQL = "select id, name_category, parent_id from category where id=";
    public static final String EXISTS_CATEGORY_BY_ID_SQL = "select exists (select * from category where id=%d)";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void isOkFindCategoryByIDTest() throws Exception {
        long id = 3L;
        Category category = new Category(id, "restaurant", 2L);
        String expectedContent = objectMapper.writeValueAsString(category);
        mockMvc.perform(get(START_URL + "/id/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isOkFindCategoryByNameTest() throws Exception {
        String name = "restaurant";
        Category category = new Category(3L, name, 2L);
        String expectedContent = objectMapper.writeValueAsString(category);
        mockMvc.perform(get(START_URL + "/name/" + name))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    void isOkInsertTest() throws Exception {
        Category categoryRequest = new Category(null, "cafe", 2L);
        String request = objectMapper.writeValueAsString(categoryRequest);
        String expectedContent = "true";
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        Category actualCategory = jdbcTemplate.queryForObject(SELECT_CATEGORY_BY_NAME_SQL + "'cafe'", getRowMapperCategory());
        assert actualCategory != null;
    }

    @Test
    @Transactional
    void isOkUpdateTest() throws Exception {
        long id = 3L;
        Category categoryRequest = new Category(id, "rest", 2L);
        String request = objectMapper.writeValueAsString(categoryRequest);
        mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
        Category actualCourse = jdbcTemplate.queryForObject(SELECT_CATEGORY_BY_ID_SQL + id, getRowMapperCategory());
        Assertions.assertEquals(categoryRequest, actualCourse);
    }

    @Test
    @Transactional
    @Sql(statements = "insert into category values (20,'test', 2, 3)")
    void isOkDeleteTest() throws Exception {
        long id = 20L;
        String expectedContent = "true";
        mockMvc.perform(delete(START_URL + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        Boolean resultDelete = jdbcTemplate.queryForObject(String.format(EXISTS_CATEGORY_BY_ID_SQL, id), Boolean.class);
        Assertions.assertEquals(Boolean.FALSE, resultDelete);
    }

    @Test
    void isNotFoundGetCategoryByIDTest() throws Exception {
        long id = 200L;
        String expectedContent = CATEGORY_NOT_FOUND + id;
        mockMvc.perform(get(START_URL + "/id/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isBusyInsetTest() throws Exception {
        String name = "bar";
        Category categoryRequest = new Category(null, "bar", 2L);
        String request = objectMapper.writeValueAsString(categoryRequest);
        String expectedContent = CATEGORY_ALREADY_CREATED + name;
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isParentNotFoundInsetTest() throws Exception {
        long parentId = 40L;
        Category categoryRequest = new Category(null, "rest", parentId);
        String request = objectMapper.writeValueAsString(categoryRequest);
        String expectedContent = CATEGORY_PARENT_NOT_FOUND_WITH_ID + parentId;
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isNotFoundUpdateTest() throws Exception {
        long id = 200L;
        Category categoryRequest = new Category(id, "rest", 2L);
        String request = objectMapper.writeValueAsString(categoryRequest);
        String expectedContent = CATEGORY_NOT_FOUND + id;
        mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isNameBusyUpdateTest() throws Exception {
        String name = "restaurant";
        Category categoryRequest = new Category(4L, name, 2L);
        String request = objectMapper.writeValueAsString(categoryRequest);
        String expectedContent = CATEGORY_ALREADY_CREATED + name;
        mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isNotFoundDeleteTest() throws Exception {
        long id = 200L;
        String expectedContent = CATEGORY_NOT_FOUND + CATEGORY_HAVE_CHILD + id;
        mockMvc.perform(delete(START_URL + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isIncorrectIdTest() throws Exception {
        long id = -1L;
        String expectedContent = "[\"id must be positive\"]";
        mockMvc.perform(get(START_URL + "/id/" + id))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isValidCreateModelTest() throws Exception {
        Category categoryRequest = new Category(10L, null, -1L);
        String request = objectMapper.writeValueAsString(categoryRequest);
        List<String> errorMessageExpected = List.of(
                "Id must be null",
                "Name can not be blank",
                "Parent id should be positive"
        );
        MvcResult result = mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
        String responseContent = result.getResponse().getContentAsString();
        List<String> responseMessages = List.of(objectMapper.readValue(responseContent, String[].class));
        Assertions.assertTrue(responseMessages.containsAll(errorMessageExpected));
    }

    @Test
    void isValidUpdateModelTest() throws Exception {
        Category categoryRequest = new Category(null, null, -1L);
        String request = objectMapper.writeValueAsString(categoryRequest);
        List<String> errorMessageExpected = List.of(
                "Id must be not null",
                "Name can not be blank",
                "Parent id should be positive"
        );
        MvcResult result = mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
        String responseContent = result.getResponse().getContentAsString();
        List<String> responseMessages = List.of(objectMapper.readValue(responseContent, String[].class));
        Assertions.assertTrue(responseMessages.containsAll(errorMessageExpected));
    }


    private RowMapper<Category> getRowMapperCategory() {
        return (rs, row) -> new Category(
                rs.getLong("id"),
                rs.getString("name_category"),
                rs.getLong("parent_id")
        );
    }
}

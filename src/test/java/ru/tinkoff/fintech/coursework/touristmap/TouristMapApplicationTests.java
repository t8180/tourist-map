package ru.tinkoff.fintech.coursework.touristmap;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest
public class TouristMapApplicationTests {

    @Container
    public static PostgreSQLContainer<?> myPSQLContainer = new PostgreSQLContainer<>("postgres:latest")
            .withDatabaseName("ddniq7tugp21cs");


    @DynamicPropertySource
    public static void overwriteProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", myPSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", myPSQLContainer::getUsername);
        registry.add("spring.datasource.password", myPSQLContainer::getPassword);
    }
}

package ru.tinkoff.fintech.coursework.touristmap.models.json;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoordinateTest {

    @Test
    void calculateDistance() {
        Coordinate coordinate1 = new Coordinate(55.414091, 37.480879);
        Coordinate coordinate2 = new Coordinate(55.439863, 37.555895);
        double exceptDistance = 5560;
        double delta = 50;
        double actualDistance = coordinate1.getDistance(coordinate2);
        assertTrue((actualDistance - delta) < exceptDistance && exceptDistance < (actualDistance + delta));
    }
}

package ru.tinkoff.fintech.coursework.touristmap.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTORequest;
import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTOResponse;
import ru.tinkoff.fintech.coursework.touristmap.TouristMapApplicationTests;
import ru.tinkoff.fintech.coursework.touristmap.models.Sight;
import ru.tinkoff.fintech.coursework.touristmap.models.json.Coordinate;
import ru.tinkoff.fintech.coursework.touristmap.models.json.VisitingTime;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "100", roles = "ADMIN")
class SightControllerTest extends TouristMapApplicationTests {
    private static final String START_URL = "/sights/";
    private static final String START_URL_ROUTE = "/sights/route";
    private static final String SIGHT_NOT_FOUND = "Sight not found with id: ";
    private static final String SIGHT_ALREADY_CREATED = "Sight is already created with name: ";
    private static final String CATEGORY_NOT_FOUND_WITH_NAME = "This category not exists: ";
    private static final String EMPTY_OPTIMAL_ROUTE = "Don't found optimal route, please change param";
    private static final String NOT_FOUND_ANY_SIGHTS = "Don't found any sights with this categories and budget";
    public static final String SELECT_SIGHT_BY_NAME_SQL = "select s.id, name, name_category, description, coordinate, website, work_time, price from sights s left join category c ON s.id_category=c.id where s.name=";
    public static final String EXISTS_SIGHT_BY_ID_SQL = "select exists (select * from sights where id=%d)";

    private final Coordinate coordinate = new Coordinate(55.757496, 37.610125);
    private final VisitingTime time = new VisitingTime(12, 3, 3);
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void isOkGetOptimalRoute() throws Exception {
        Long[] expectedArr = {19L, 3L, 18L, 4L};
        List<String> nameSightList = List.of("bar", "theatre");
        SightDTORequest sightDTO = new SightDTORequest(nameSightList, 9.0, 3.0, coordinate, 20000);
        String request = objectMapper.writeValueAsString(sightDTO);
        MvcResult result = mockMvc.perform(post(START_URL_ROUTE)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andReturn();
        String responseContent = result.getResponse().getContentAsString();
        List<SightDTOResponse> responseList = List.of(objectMapper.readValue(responseContent, SightDTOResponse[].class));
        List<Long> sightsId = new LinkedList<>();
        responseList.forEach(
                s -> sightsId.add(s.getSight().getId())
        );
        Assertions.assertArrayEquals(expectedArr, sightsId.toArray());
    }

    @Test
    void isNoAnySightsForRoute() throws Exception {
        List<String> nameSightList = List.of("bar");
        SightDTORequest sightDTO = new SightDTORequest(nameSightList, null, null, coordinate, 0);
        String request = objectMapper.writeValueAsString(sightDTO);
        mockMvc.perform(post(START_URL_ROUTE)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(NOT_FOUND_ANY_SIGHTS));
    }

    @Test
    void isEmptyOptimalRoute() throws Exception {
        List<String> nameSightList = List.of("bar");
        Coordinate coordinateFar = new Coordinate(30, 30);
        SightDTORequest sightDTO = new SightDTORequest(nameSightList, null, null, coordinateFar, 7000);
        String request = objectMapper.writeValueAsString(sightDTO);
        mockMvc.perform(post(START_URL_ROUTE)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(EMPTY_OPTIMAL_ROUTE));
    }

    @Test
    void isValidateSightDto() throws Exception {
        Coordinate coordinate = new Coordinate(100, -200);
        SightDTORequest sightDTO = new SightDTORequest(null, -1d, 25d, coordinate, -1);
        List<String> errorMessageExpected = List.of(
                "Category name list can not be empty",
                "Time should not be less than 0",
                "latitude not be greater than 90",
                "longitude should not be less than -180",
                "Time should not be greater than 24",
                "Budget should be positive or zero"
        );
        String request = objectMapper.writeValueAsString(sightDTO);
        MvcResult result = mockMvc.perform(post(START_URL_ROUTE)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
        String responseContent = result.getResponse().getContentAsString();
        List<String> responseMessages = List.of(objectMapper.readValue(responseContent, String[].class));
        Assertions.assertTrue(responseMessages.containsAll(errorMessageExpected));
    }

    @Test
    void isOkFindSightByIDTest() throws Exception {
        long id = 3L;
        Sight sight = new Sight(id, "Timeless", "bar", "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        MvcResult result = mockMvc.perform(get(START_URL + "id/" + id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andReturn();
        String context = result.getResponse().getContentAsString();
        Sight actualSight = objectMapper.readValue(context, Sight.class);
        Assertions.assertEquals(sight, actualSight);
    }

    @Test
    void isOkFindSightByNameTest() throws Exception {
        String name = "Timeless";
        Sight sight = new Sight(3L, name, "bar", "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        MvcResult result = mockMvc.perform(get(START_URL + "name/" + name))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andReturn();
        String context = result.getResponse().getContentAsString();
        Sight actualSight = objectMapper.readValue(context, Sight.class);
        Assertions.assertEquals(sight, actualSight);
    }

    @Test
    @Transactional
    void isOkInsertTest() throws Exception {
        String expectedContent = "true";
        String name = "Time";
        Sight sightRequest = new Sight(null, name, "bar", "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        String request = objectMapper.writeValueAsString(sightRequest);
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        Sight actualStudent = jdbcTemplate.queryForObject(SELECT_SIGHT_BY_NAME_SQL + "'Time'", getRowMapperSight());
        assert actualStudent != null;
        Assertions.assertEquals(name, actualStudent.getName());
    }

    @Test
    @Transactional
    void isOkUpdateTest() throws Exception {
        long id = 3L;
        String expectedContent = "true";
        Sight sightRequest = new Sight(id, "Time", "bar", "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        String request = objectMapper.writeValueAsString(sightRequest);
        mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        Sight actualStudent = jdbcTemplate.queryForObject(SELECT_SIGHT_BY_NAME_SQL + "'Time'", getRowMapperSight());
        Assertions.assertEquals(sightRequest, actualStudent);
    }

    @Test
    @Transactional
    void isOkDeleteTest() throws Exception {
        long id = 3L;
        String expectedContent = "true";
        mockMvc.perform(delete(START_URL + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        Boolean resultDelete = jdbcTemplate.queryForObject(String.format(EXISTS_SIGHT_BY_ID_SQL, id), Boolean.class);
        Assertions.assertEquals(Boolean.FALSE, resultDelete);
    }

    @Test
    void isNotFoundGetSightByIDTest() throws Exception {
        long id = 200L;
        String expectedContent = SIGHT_NOT_FOUND + id;
        mockMvc.perform(get(START_URL + "id/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isBusyInsetTest() throws Exception {
        String type = "bar";
        String name = "Timeless";
        Sight sightRequest = new Sight(null, name, type, "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        String expectedContent = SIGHT_ALREADY_CREATED + name;
        String request = objectMapper.writeValueAsString(sightRequest);
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isCategoryNotFoundInsertTest() throws Exception {
        String categoryName = "ba";
        Sight sightRequest = new Sight(null, "Timeless", categoryName, "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        String expectedContent = CATEGORY_NOT_FOUND_WITH_NAME + categoryName;
        String request = objectMapper.writeValueAsString(sightRequest);
        mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isNotFoundUpdateTest() throws Exception {
        long id = 40L;
        String type = "bar";
        Sight sightRequest = new Sight(id, "Timeless", type, "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        String expectedContent = SIGHT_NOT_FOUND + id;
        String request = objectMapper.writeValueAsString(sightRequest);
        mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isInvalidCategoryUpdateTest() throws Exception {
        long id = 3L;
        String categoryName = "ba";
        Sight sightRequest = new Sight(id, "Timeless", categoryName, "Кальян-бар, бар, паб", coordinate, "timeless.club", time, 5000);
        String expectedContent = CATEGORY_NOT_FOUND_WITH_NAME + categoryName;
        String request = objectMapper.writeValueAsString(sightRequest);
        mockMvc.perform(put(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isNotFoundDeleteTest() throws Exception {
        long id = 200L;
        String expectedContent = SIGHT_NOT_FOUND + id;
        mockMvc.perform(delete(START_URL + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));
    }


    @Test
    void isIncorrectIdTest() throws Exception {
        long id = -1L;
        String expectedContent = "[\"id must be positive\"]";
        mockMvc.perform(delete(START_URL + id))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void isValidSightModelTest() throws Exception {
        Coordinate coordinate = new Coordinate(100, -200);
        VisitingTime visitingTime = new VisitingTime(-1, 25, -1);
        Sight sightRequest = new Sight(1L, "", "", "", coordinate, "", visitingTime, -1);
        List<String> errorMessageExpected = List.of(
                "Id must be null",
                "Name can not be blank",
                "Category can not be blank",
                "latitude not be greater than 90",
                "longitude should not be less than -180",
                "Time should not be less than 0",
                "Time should not be greater than 24",
                "averageTime should be positive",
                "Price should be positive"
        );
        String request = objectMapper.writeValueAsString(sightRequest);
        MvcResult result = mockMvc.perform(post(START_URL)
                        .contentType(APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
        String responseContent = result.getResponse().getContentAsString();
        List<String> responseMessages = List.of(objectMapper.readValue(responseContent, String[].class));
        Assertions.assertTrue(responseMessages.containsAll(errorMessageExpected));
    }

    private RowMapper<Sight> getRowMapperSight() {
        return (rs, row) -> {
            try {
                return new Sight(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("name_category"),
                        rs.getString("description"),
                        objectMapper.readValue(rs.getString("coordinate"), Coordinate.class),
                        rs.getString("website"),
                        objectMapper.readValue(rs.getString("work_time"), VisitingTime.class),
                        rs.getInt("price")
                );
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }
        };
    }
}

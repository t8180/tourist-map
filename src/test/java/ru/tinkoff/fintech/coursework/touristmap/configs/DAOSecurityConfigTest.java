package ru.tinkoff.fintech.coursework.touristmap.configs;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.fintech.coursework.touristmap.TouristMapApplicationTests;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@SpringBootTest
@DirtiesContext
class DAOSecurityConfigTest extends TouristMapApplicationTests {
    private static final String CATEGORY_URL = "/category/1";
    private static final String SIGHT_URL = "/sights/1";

    @Autowired
    private MockMvc mockMvc;


    @Test
    void isInvalidLoginSecurityTest() throws Exception {
        mockMvc.perform(get(SIGHT_URL)
                        .with(httpBasic("log1", "100")))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "log", password = "100")
    void isInvalidRoleCategorySecurityTest() throws Exception {
        mockMvc.perform(delete(CATEGORY_URL))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "log", password = "100")
    void isInvalidRoleSightSecurityTest() throws Exception {
        mockMvc.perform(delete(SIGHT_URL))
                .andExpect(status().isForbidden());
    }
}

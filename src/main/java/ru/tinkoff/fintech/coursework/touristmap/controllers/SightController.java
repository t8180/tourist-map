package ru.tinkoff.fintech.coursework.touristmap.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTORequest;
import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTOResponse;
import ru.tinkoff.fintech.coursework.touristmap.models.Sight;
import ru.tinkoff.fintech.coursework.touristmap.service.SightsService;

import java.util.List;

@RestController
@RequestMapping("/sights")
@RequiredArgsConstructor
public class SightController {
    private final SightsService sightsService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public List<Sight> readAllSight() {
        return sightsService.readAllSights();
    }

    @GetMapping("/id/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public Sight readSightById(@PathVariable Long id) {
        return sightsService.readSightById(id);
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public Sight readSightByName(@PathVariable String name) {
        return sightsService.readSightByName(name);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createSight(@RequestBody Sight sight) {
        return sightsService.createSight(sight);
    }

    @PostMapping("/route")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public List<SightDTOResponse> getOptimalRoute(@RequestBody SightDTORequest sightDTORequest) {
        return sightsService.getOptimalRoute(sightDTORequest);
    }

    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    public boolean updateSight(@RequestBody Sight sight) {
        return sightsService.updateSight(sight);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean deleteSightById(@PathVariable Long id) {
        return sightsService.deleteSight(id);
    }
}

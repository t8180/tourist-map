package ru.tinkoff.fintech.coursework.touristmap.repository;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import ru.tinkoff.fintech.coursework.touristmap.models.Category;

import java.util.List;

@Repository
@Mapper
public interface CategoryRepository {
    @Select("select id, name_category, parent_id from category where name_category=#{name};")
    @Results(id = "category", value = {
            @Result(property = "nameCategory", column = "name_category"),
            @Result(property = "parentId", column = "parent_id")
    })
    Category selectCategoryByName(String name);

    @Select("select id, name_category, parent_id from category where id=#{id};")
    @ResultMap("category")
    Category selectCategoryById(long id);

    @Select("select id, name_category, parent_id from category")
    @ResultMap("category")
    List<Category> selectAllCategories();

    @Select("select exists(select * from category where id=#{id})")
    boolean checkExistsCategoryById(long id);

    @Select("select exists(select * from category where name_category=#{name})")
    boolean checkExistsCategoryByName(String name);

    @Insert("insert into category (name_category, parent_id, level) " +
            "(select #{nameCategory}, #{parentId}, c.level+1 from category c where c.id=#{parentId})" +
            "ON CONFLICT (name_category) DO NOTHING;")
    boolean insertCategory(Category category);

    @Update("update category set name_category=#{nameCategory}, parent_id=#{parentId}, " +
            "level=(select s.level+1 from category s where id=#{parentId})  " +
            "where id=#{id} " +
            "and not exists(select * from category where id!=#{id} and name_category=#{nameCategory})")
    boolean updateCategory(Category category);

    @Delete("delete from category where id=#{id}")
    boolean deleteCategoryById(long id);
}

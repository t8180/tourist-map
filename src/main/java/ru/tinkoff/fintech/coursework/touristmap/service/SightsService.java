package ru.tinkoff.fintech.coursework.touristmap.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTORequest;
import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTOResponse;
import ru.tinkoff.fintech.coursework.touristmap.algorithm.Algorithm;
import ru.tinkoff.fintech.coursework.touristmap.exeptions.InvalidDataException;
import ru.tinkoff.fintech.coursework.touristmap.models.Sight;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.Exists;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.New;
import ru.tinkoff.fintech.coursework.touristmap.repository.SightsRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.List;

import static ru.tinkoff.fintech.coursework.touristmap.service.validators.OnlyPositiveSqlResultValidator.isValid;

@Service
@RequiredArgsConstructor
@Validated
public class SightsService {
    private static final String SIGHT_NOT_FOUND_ID = "Sight not found with id: ";
    private static final String SIGHT_NOT_FOUND_NAME = "Sight not found with name: ";
    private static final String SIGHT_ALREADY_CREATED = "Sight is already created with name: ";
    private static final String EMPTY_OPTIMAL_ROUTE = "Don't found optimal route, please change param";
    private static final String NOT_FOUND_ANY_SIGHTS = "Don't found any sights with this categories and budget";
    private static final String NOT_FOUND_CATEGORY = "This category not exists: ";
    private final SightsRepository sightsRepository;
    private final CategoryService categoryService;

    public List<Sight> readAllSights() {
        return sightsRepository.findAllSights();
    }

    public Sight readSightById(@Positive(message = "id must be positive") long id) {
        Sight sight = sightsRepository.findSightByID(id);
        isValid(sight, SIGHT_NOT_FOUND_ID + id);
        return sight;
    }

    public Sight readSightByName(@NotBlank(message = "Name can not be blank") String name) {
        Sight sight = sightsRepository.findSightByName(name);
        isValid(sight, SIGHT_NOT_FOUND_NAME + name);
        return sight;
    }

    @Validated(New.class)
    public boolean createSight(@Valid Sight sight) {
        isCategoryExists(sight.getCategory());
        boolean res = sightsRepository.insertSight(sight);
        isValid(res, SIGHT_ALREADY_CREATED + sight.getName());
        return res;
    }

    @Validated(Exists.class)
    public boolean updateSight(@Valid Sight sight) {
        boolean res = sightsRepository.updateSight(sight);
        if (!res) {
            isCategoryExists(sight.getCategory());
            throw new InvalidDataException(SIGHT_NOT_FOUND_ID + sight.getId());
        }
        return true;
    }

    public boolean deleteSight(@Positive(message = "id must be positive") Long id) {
        boolean res = sightsRepository.deleteSight(id);
        isValid(res, SIGHT_NOT_FOUND_ID + id);
        return res;
    }

    public List<SightDTOResponse> getOptimalRoute(@Valid SightDTORequest sightDTO) {
        List<Sight> sightList = sightsRepository.findSightsByCategoryList(sightDTO.getCategoryNameList(), sightDTO.getBudget());
        if (sightList.isEmpty()) {
            throw new InvalidDataException(NOT_FOUND_ANY_SIGHTS);
        }
        List<SightDTOResponse> optimalSightList = Algorithm.getOptimalRoute(sightList, sightDTO);
        if (optimalSightList.isEmpty()) {
            throw new InvalidDataException(EMPTY_OPTIMAL_ROUTE);
        }
        return optimalSightList;
    }

    private void isCategoryExists(String nameCategory) {
        boolean exists = categoryService.isCategoryExistsByName(nameCategory);
        if (!exists) throw new InvalidDataException(NOT_FOUND_CATEGORY + nameCategory);
    }
}

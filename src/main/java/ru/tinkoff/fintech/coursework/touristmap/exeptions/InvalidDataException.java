package ru.tinkoff.fintech.coursework.touristmap.exeptions;

public class InvalidDataException extends RuntimeException {
    public InvalidDataException(String message) {
        super(message);
    }
}

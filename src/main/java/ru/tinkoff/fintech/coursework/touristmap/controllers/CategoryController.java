package ru.tinkoff.fintech.coursework.touristmap.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.coursework.touristmap.models.Category;
import ru.tinkoff.fintech.coursework.touristmap.service.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public List<Category> getAllCategories() {
        return categoryService.readAllCategories();
    }

    @GetMapping("/id/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public Category readCategoryById(@PathVariable Long id) {
        return categoryService.readCategoryById(id);
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public Category readCategoryByName(@PathVariable String name) {
        return categoryService.readCategoryByName(name);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createCategory(@RequestBody Category category) {
        return categoryService.createCategory(category);
    }

    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    public boolean updateCategory(@RequestBody Category category) {
        return categoryService.updateCategory(category);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean deleteCategoryById(@PathVariable Long id) {
        return categoryService.deleteCategoryById(id);
    }
}

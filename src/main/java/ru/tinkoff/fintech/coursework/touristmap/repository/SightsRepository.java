package ru.tinkoff.fintech.coursework.touristmap.repository;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import ru.tinkoff.fintech.coursework.touristmap.models.Sight;

import java.util.List;

@Repository
@Mapper
public interface SightsRepository {

    @Select({"<script>",
            "WITH RECURSIVE r AS( " +
                    "select id, name_category, level from category  where name_category IN " +
                    "<foreach item='name' collection='categoriesList' open='(' separator=',' close=') '> " +
                    "#{name, jdbcType=VARCHAR} " +
                    "</foreach> " +
                    "union all " +
                    "select c.id, c.name_category, c.level " +
                    "from category c " +
                    "join r ON c.parent_id=r.id " +
                    ") " +
                    "SELECT s.id, name, name_category, description, coordinate, website, work_time, price from sights s " +
                    "right join r ON r.id=s.id_category where r.level=3 and price &lt;=#{price} ",
            "</script>"
    })
    @ResultMap("sights")
    List<Sight> findSightsByCategoryList(@Param("categoriesList") List<String> categories, int price);

    @Select("select s.id, name, name_category, description, coordinate, website, work_time, price from sights s " +
            "left join category c ON s.id_category=c.id")
    @Results(id = "sights", value = {
            @Result(property = "id", column = "s.id"),
            @Result(property = "name", column = "name"),
            @Result(property = "category", column = "name_category"),
            @Result(property = "workTime", column = "work_time")
    }
    )
    List<Sight> findAllSights();

    @Select("select s.id, name, name_category, description, coordinate, website, work_time, price from sights s " +
            "left join category c ON s.id_category=c.id where s.id=#{id} ")
    @ResultMap("sights")
    Sight findSightByID(long id);

    @Select("select s.id, name, name_category, description, coordinate, website, work_time, price from sights s " +
            "left join category c ON s.id_category=c.id where s.name=#{name} ")
    @ResultMap("sights")
    Sight findSightByName(String name);

    @Insert("INSERT INTO sights (name, coordinate, description, website, work_time, price, id_category) " +
            "(select #{name}, #{coordinate}, #{description}, #{website}, #{workTime}, #{price}, " +
            "id from category where name_category=#{category} and level=3)" +
            "ON CONFLICT (name) DO NOTHING;")
    boolean insertSight(Sight sight);

    @Update("UPDATE sights set name=#{name}, coordinate=#{coordinate}, description=#{description}, " +
            "website=#{website}, work_time=#{workTime}, price=#{price}," +
            "id_category=(select id from category where name_category=#{category} and level=3 ) " +
            "where exists(select id from category where name_category=#{category} and level=3) and id=#{id} ")
    boolean updateSight(Sight sight);

    @Delete("DELETE FROM sights WHERE id = #{id}")
    boolean deleteSight(long id);
}

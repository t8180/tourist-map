package ru.tinkoff.fintech.coursework.touristmap.repository.handler;

import org.apache.ibatis.type.MappedTypes;
import ru.tinkoff.fintech.coursework.touristmap.models.json.VisitingTime;

@MappedTypes({VisitingTime.class})
public class VisitingTimeHandler extends JsonTypeHandler<VisitingTime> {
}

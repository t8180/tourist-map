package ru.tinkoff.fintech.coursework.touristmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TouristMapApplication {

    public static void main(String[] args) {
        SpringApplication.run(TouristMapApplication.class, args);
    }
}

package ru.tinkoff.fintech.coursework.touristmap.models;

import lombok.ToString;
import lombok.Value;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.Exists;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.New;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;
import java.util.Objects;

@Value
@ToString
public class Category {
    @Null(message = "Id must be null",
            groups = New.class)
    @Positive(message = "Id must be positive",
            groups = Exists.class)
    @NotNull(message = "Id must be not null",
            groups = Exists.class)
    Long id;
    @NotBlank(message = "Name can not be blank",
            groups = {New.class, Exists.class})
    String nameCategory;
    @Positive(message = "Parent id should be positive",
            groups = {New.class, Exists.class})
    Long parentId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(id, category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

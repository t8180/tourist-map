package ru.tinkoff.fintech.coursework.touristmap.service.validators;

import ru.tinkoff.fintech.coursework.touristmap.exeptions.InvalidDataException;

public class OnlyPositiveSqlResultValidator {
    private OnlyPositiveSqlResultValidator() {
    }

    public static void isValid(Object o, String errorMessage) {
        if (o == null)
            throw new InvalidDataException(errorMessage);
        if (o instanceof Integer res) {
            if (res == 0)
                throw new InvalidDataException(errorMessage);
        } else if (o instanceof Boolean bool && Boolean.FALSE.equals(bool)) {
            throw new InvalidDataException(errorMessage);
        }
    }
}

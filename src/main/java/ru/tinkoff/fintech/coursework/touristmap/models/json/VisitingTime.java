package ru.tinkoff.fintech.coursework.touristmap.models.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.Exists;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.New;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

@Getter
@ToString
public class VisitingTime {
    private static final int HOUR_IN_DAY = 24;
    @Min(value = 0, message = "Time should not be less than 0",
            groups = {New.class, Exists.class})
    @Max(value = 24, message = "Time should not be greater than 24",
            groups = {New.class, Exists.class})
    private final double startWorkTime;
    @Min(value = 0, message = "Time should not be less than 0",
            groups = {New.class, Exists.class})
    @Max(value = 24, message = "Time should not be greater than 24",
            groups = {New.class, Exists.class})
    private final double endWorkTime;
    @Positive(message = "averageTime should be positive",
            groups = {New.class, Exists.class})
    private final double averageTimeForVisitSight;

    @JsonCreator
    public VisitingTime(@JsonProperty("startWorkTime") double startWorkTime,
                        @JsonProperty("endWorkTime") double endWorkTime,
                        @JsonProperty("averageTime") double averageTime) {
        this.startWorkTime = startWorkTime;
        this.endWorkTime = endWorkTime;
        this.averageTimeForVisitSight = averageTime;
    }

    public boolean isWork(double time) {
        double realEndWorkTime = endWorkTime <= startWorkTime ? endWorkTime + HOUR_IN_DAY : endWorkTime;
        return startWorkTime <= time && time <= realEndWorkTime;
    }
}

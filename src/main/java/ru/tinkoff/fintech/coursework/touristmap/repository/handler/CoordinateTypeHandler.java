package ru.tinkoff.fintech.coursework.touristmap.repository.handler;

import org.apache.ibatis.type.MappedTypes;
import ru.tinkoff.fintech.coursework.touristmap.models.json.Coordinate;

@MappedTypes({Coordinate.class})
public class CoordinateTypeHandler extends JsonTypeHandler<Coordinate> {
}

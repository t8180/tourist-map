package ru.tinkoff.fintech.coursework.touristmap.models;

import lombok.ToString;
import lombok.Value;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.Exists;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.New;
import ru.tinkoff.fintech.coursework.touristmap.models.json.Coordinate;
import ru.tinkoff.fintech.coursework.touristmap.models.json.VisitingTime;

import javax.validation.Valid;
import javax.validation.constraints.*;

import java.util.Objects;

@Value
@ToString
public class Sight {
    @Null(message = "Id must be null",
            groups = New.class)
    @Positive(message = "Id must be positive",
            groups = Exists.class)
    @NotNull(message = "Id must be not null",
            groups = Exists.class)
    Long id;
    @NotBlank(message = "Name can not be blank",
            groups = {New.class, Exists.class})
    String name;
    @NotBlank(message = "Category can not be blank",
            groups = {New.class, Exists.class})
    String category;
    String description;
    @Valid
    @NotNull(message = "coordinate can not be null",
            groups = {New.class, Exists.class})
    Coordinate coordinate;
    String website;
    @Valid
    @NotNull(message = "workTime can not be null",
            groups = {New.class, Exists.class})
    VisitingTime workTime;
    @PositiveOrZero(message = "Price should be positive",
            groups = {New.class, Exists.class})
    int price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sight sight = (Sight) o;
        return Objects.equals(id, sight.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

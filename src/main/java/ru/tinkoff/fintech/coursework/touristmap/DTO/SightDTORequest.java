package ru.tinkoff.fintech.coursework.touristmap.DTO;

import lombok.ToString;
import lombok.Value;
import ru.tinkoff.fintech.coursework.touristmap.models.json.Coordinate;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Value
@ToString
public class SightDTORequest {
    @NotEmpty(message = "Category name list can not be empty")
    List<String> categoryNameList;
    @Min(value = 0, message = "Time should not be less than 0")
    @Max(value = 24, message = "Time should not be greater than 24")
    Double startTime;
    @Min(value = 0, message = "Time should not be less than 0")
    @Max(value = 24, message = "Time should not be greater than 24")
    Double finishTime;
    @NotNull(message = "coordinate can not be null")
    @Valid
    Coordinate startCoordinate;
    @PositiveOrZero(message = "Budget should be positive or zero")
    int budget;
}

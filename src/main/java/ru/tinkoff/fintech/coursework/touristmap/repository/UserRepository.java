package ru.tinkoff.fintech.coursework.touristmap.repository;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import ru.tinkoff.fintech.coursework.touristmap.DTO.UserDTO;
import ru.tinkoff.fintech.coursework.touristmap.models.User;

import java.util.List;

@Repository
@Mapper
public interface UserRepository {
    @Select("SELECT * FROM users WHERE username=#{username}")
    @Result(property = "role", column = "user_role")
    User findUserByUserName(String username);

    @Select("SELECT id, username, user_role FROM users WHERE id=#{id}")
    @Result(property = "role", column = "user_role")
    UserDTO findUserById(long id);

    @Select("SELECT id, username, user_role from users")
    @Result(property = "role", column = "user_role")
    List<UserDTO> findAllUsers();

    @Insert("INSERT  INTO users (username, password, user_role) " +
            " VALUES (#{username}, #{password}, #{role})" +
            "ON CONFLICT (username) DO NOTHING; "
    )
    boolean insertUser(User user);

    @Update("UPDATE users set username=#{username}, password=#{password}, user_role=#{role} " +
            "where id=#{id}")
    boolean updateUser(User user);

    @Delete("DELETE FROM users where id=#{id}")
    boolean deleteUserById(long id);
}

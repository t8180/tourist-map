package ru.tinkoff.fintech.coursework.touristmap.DTO;

import lombok.ToString;
import lombok.Value;
import ru.tinkoff.fintech.coursework.touristmap.models.Sight;

import java.util.Objects;

@Value
@ToString
public class SightDTOResponse {
    Sight sight;
    double distance;
    double startTime;
    double finishTime;
    int remainingBudget;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SightDTOResponse that = (SightDTOResponse) o;
        return Objects.equals(sight, that.sight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sight);
    }
}

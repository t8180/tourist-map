package ru.tinkoff.fintech.coursework.touristmap.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.coursework.touristmap.DTO.UserDTO;
import ru.tinkoff.fintech.coursework.touristmap.models.User;
import ru.tinkoff.fintech.coursework.touristmap.repository.UserRepository;

import java.util.Collection;
import java.util.List;

import static ru.tinkoff.fintech.coursework.touristmap.service.validators.OnlyPositiveSqlResultValidator.isValid;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private static final String USER_NOT_FOUND_WITH_NAME = "User not found with name: ";
    private static final String USER_NOT_FOUND_WITH_ID = "User not found with id: ";
    private static final String USER_ALREADY_EXISTS = "User already exists with name: ";
    private final UserRepository userRepository;


    private User findByUsername(String username) {
        return userRepository.findUserByUserName(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = findByUsername(username);
        isValid(user, USER_NOT_FOUND_WITH_NAME + username);
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), mapRolesToAuthorities(user.getRole().name()));
    }

    public List<UserDTO> findAllUsers() {
        return userRepository.findAllUsers();
    }

    public UserDTO findUserById(Long id) {
        return userRepository.findUserById(id);
    }

    public boolean createUser(User user) {
        boolean res = userRepository.insertUser(user);
        isValid(res, USER_ALREADY_EXISTS + user.getUsername());
        return true;
    }

    public boolean updateUser(User user) {
        boolean res = userRepository.updateUser(user);
        isValid(res, USER_NOT_FOUND_WITH_ID + user.getId());
        return res;
    }

    public boolean deleteUserById(Long id) {
        boolean res = userRepository.deleteUserById(id);
        isValid(res, USER_NOT_FOUND_WITH_ID + id);
        return res;
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(String roleName) {
        return List.of(new SimpleGrantedAuthority(roleName));
    }
}

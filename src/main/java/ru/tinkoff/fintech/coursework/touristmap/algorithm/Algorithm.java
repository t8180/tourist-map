package ru.tinkoff.fintech.coursework.touristmap.algorithm;

import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTORequest;
import ru.tinkoff.fintech.coursework.touristmap.DTO.SightDTOResponse;
import ru.tinkoff.fintech.coursework.touristmap.models.Sight;
import ru.tinkoff.fintech.coursework.touristmap.models.json.Coordinate;

import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;
import java.util.Optional;
import java.util.Comparator;
import java.util.stream.Collectors;

public class Algorithm {
    private static final int VELOCITY_METER_IN_HOUR = 6000;
    private static final int HOUR_IN_DAY = 24;

    private Algorithm() {
    }

    public static List<SightDTOResponse> getOptimalRoute(List<Sight> sights, SightDTORequest sightDTO) {
        List<SightDTOResponse> optionalSight = new LinkedList<>();
        int budget = sightDTO.getBudget();
        double nowTime = sightDTO.getStartTime() == null ? 0 : sightDTO.getStartTime();
        double finishTime = sightDTO.getFinishTime() == null ? HOUR_IN_DAY : sightDTO.getFinishTime();
        if (finishTime < nowTime) {
            finishTime += HOUR_IN_DAY;
        }
        Coordinate nowCoordinate = sightDTO.getStartCoordinate();
        Set<Sight> sightSet = new HashSet<>(sights);
        while (!sightSet.isEmpty()) {
            sightSet = getFilteredSights(sightSet, budget, nowTime, finishTime, nowCoordinate);
            if (!sightSet.isEmpty()) {
                Optional<Sight> addSightOption = getFirstWhichWork(sightSet, nowTime, nowCoordinate);
                if (addSightOption.isPresent()) {
                    Sight addSight = addSightOption.get();
                    budget -= addSight.getPrice();
                    SightDTOResponse sightDTOResponse = createSightDTOResponse(addSight, nowCoordinate, nowTime, budget);
                    optionalSight.add(sightDTOResponse);
                    nowTime = calculateTimeAfterVisitingSight(addSight, nowTime, nowCoordinate);
                    nowCoordinate = addSight.getCoordinate();
                    sightSet.remove(addSight);
                } else nowTime++;
            }
        }
        return optionalSight;
    }

    private static SightDTOResponse createSightDTOResponse(Sight addSight, Coordinate nowCoordinate, double nowTime, int budget) {
        double distance = round(addSight.getCoordinate().getDistance(nowCoordinate));
        double timeTravel = calculateTimeAfterVisitingSight(addSight, nowTime, nowCoordinate);
        if (timeTravel > HOUR_IN_DAY) {
            timeTravel -= HOUR_IN_DAY;
        }
        return new SightDTOResponse(addSight, distance, round(nowTime), round(timeTravel), budget);
    }

    private static double round(double d) {
        return Math.round(d * 100) / 100.0;
    }

    private static Optional<Sight> getFirstWhichWork(Set<Sight> sightSet, double nowTime, Coordinate nowCoordinate) {
        return sightSet.stream()
                .filter(sight -> isSightWork(sight, nowTime, nowCoordinate))
                .min(Comparator.comparing(sight -> calculateDistance(sight, nowCoordinate)));
    }

    private static Set<Sight> getFilteredSights(Set<Sight> sightSet, int budget, double nowTime, double finishTime, Coordinate nowCoordinate) {
        return sightSet.stream()
                .filter(sight -> isEnoughBudget(sight, budget))
                .filter(sight -> isCanDoItInTime(sight, nowTime, finishTime, nowCoordinate))
                .collect(Collectors.toSet());
    }

    private static boolean isCanDoItInTime(Sight sight, double nowTime, double finishTime, Coordinate nowCoordinate) {
        return calculateTimeAfterVisitingSight(sight, nowTime, nowCoordinate) <= finishTime;
    }

    private static double calculateTimeAfterVisitingSight(Sight sight, double nowTime, Coordinate coordinate) {
        return nowTime
                + calculateDistance(sight, coordinate) / VELOCITY_METER_IN_HOUR
                + sight.getWorkTime().getAverageTimeForVisitSight();
    }

    private static boolean isSightWork(Sight sight, double nowTime, Coordinate coordinate) {
        double endTimeForThisSight = nowTime + calculateDistance(sight, coordinate) / VELOCITY_METER_IN_HOUR;
        return sight.getWorkTime().isWork(endTimeForThisSight);
    }

    private static boolean isEnoughBudget(Sight sight, int budget) {
        return budget - sight.getPrice() >= 0;
    }

    private static double calculateDistance(Sight sight, Coordinate coordinate) {
        return coordinate.getDistance(sight.getCoordinate());
    }
}

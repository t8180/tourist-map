package ru.tinkoff.fintech.coursework.touristmap.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.coursework.touristmap.DTO.UserDTO;
import ru.tinkoff.fintech.coursework.touristmap.models.User;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.Exists;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.New;
import ru.tinkoff.fintech.coursework.touristmap.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@Validated
public class UsersController {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Validated(New.class)
    public boolean registerUser(@RequestBody @Valid User user) {
        String hashPassword = passwordEncoder.encode(user.getPassword());
        User saveUser = new User(null, user.getUsername(), hashPassword, user.getRole());
        return userService.createUser(saveUser);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public UserDTO findUserById(@PathVariable @Positive(message = "id must be positive") Long id) {
        return userService.findUserById(id);
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserDTO> findAllUsers() {
        return userService.findAllUsers();
    }


    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Validated(Exists.class)
    public boolean updateUser(@RequestBody @Valid User user) {
        String hashPassword = passwordEncoder.encode(user.getPassword());
        User saveUser = new User(user.getId(), user.getUsername(), hashPassword, user.getRole());
        return userService.updateUser(saveUser);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean deleteUserById(@PathVariable @Positive(message = "id must be positive") Long id) {
        return userService.deleteUserById(id);
    }
}

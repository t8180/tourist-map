package ru.tinkoff.fintech.coursework.touristmap.models.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.Exists;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.New;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.groups.Default;

import java.util.Objects;

import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static java.lang.Math.acos;
import static java.lang.Math.toRadians;

@Getter
@ToString
public class Coordinate {
    private static final int RADIUS_EARTH = 6372795;
    @Min(value = -90, message = "latitude not be less than -90",
            groups = {New.class, Exists.class, Default.class})
    @Max(value = 90, message = "latitude not be greater than 90",
            groups = {New.class, Exists.class, Default.class})
    private final double latitude;
    @Min(value = -180, message = "longitude should not be less than -180",
            groups = {New.class, Exists.class, Default.class})
    @Max(value = 180, message = "longitude should not be greater than 180",
            groups = {New.class, Exists.class, Default.class})
    private final double longitude;

    @JsonCreator
    public Coordinate(@JsonProperty("latitude") double latitude,
                      @JsonProperty("longitude") double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Coordinate that = (Coordinate) o;
        return java.lang.Double.compare(that.longitude, longitude) == 0 && java.lang.Double.compare(that.latitude, latitude) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), longitude, latitude);
    }

    public double getDistance(Coordinate coordinate) {
        double radThisLat = toRadians(latitude);
        double radLat = toRadians(coordinate.getLatitude());
        double radThisLong = toRadians(longitude);
        double radLong = toRadians(coordinate.getLongitude());
        double angularDistance = acos(sin(radThisLat) * sin(radLat) + cos(radThisLat) * cos(radLat) * cos(radLong - radThisLong));
        return RADIUS_EARTH * angularDistance;
    }
}

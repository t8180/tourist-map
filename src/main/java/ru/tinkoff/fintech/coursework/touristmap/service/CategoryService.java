package ru.tinkoff.fintech.coursework.touristmap.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import ru.tinkoff.fintech.coursework.touristmap.exeptions.InvalidDataException;
import ru.tinkoff.fintech.coursework.touristmap.models.Category;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.Exists;
import ru.tinkoff.fintech.coursework.touristmap.models.groups.New;
import ru.tinkoff.fintech.coursework.touristmap.repository.CategoryRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import java.util.List;

import static ru.tinkoff.fintech.coursework.touristmap.service.validators.OnlyPositiveSqlResultValidator.isValid;

@Service
@RequiredArgsConstructor
@Validated
public class CategoryService {
    private static final String CATEGORY_NOT_FOUND_WITH_ID = "Category not found with id: ";
    private static final String CATEGORY_PARENT_NOT_FOUND_WITH_ID = "Category parent not found with id: ";
    private static final String CATEGORY_NOT_FOUND_WITH_NAME = "Category not found with name: ";
    private static final String CATEGORY_HAVE_CHILD = " Or category have child";
    private static final String CATEGORY_ALREADY_CREATED = "Category is already created with name: ";

    private final CategoryRepository categoryRepository;

    public Category readCategoryById(@Positive(message = "id must be positive") Long id) {
        Category category = categoryRepository.selectCategoryById(id);
        isValid(category, CATEGORY_NOT_FOUND_WITH_ID + id);
        return category;
    }

    public Category readCategoryByName(@NotBlank(message = "Name can not be blank") String name) {
        Category category = categoryRepository.selectCategoryByName(name);
        isValid(category, CATEGORY_NOT_FOUND_WITH_NAME + name);
        return category;
    }

    public List<Category> readAllCategories() {
        return categoryRepository.selectAllCategories();
    }

    @Validated(New.class)
    public boolean createCategory(@Valid Category category) {
        boolean res = categoryRepository.insertCategory(category);
        if (!res) {
            isParentExists(category.getParentId());
            throw new InvalidDataException(CATEGORY_ALREADY_CREATED + category.getNameCategory());
        }
        return true;
    }

    @Validated({Exists.class})
    public boolean updateCategory(@Valid Category category) {
        boolean res = categoryRepository.updateCategory(category);
        if (!res) {
            isCategoryExistsById(category.getId());
            isParentExists(category.getParentId());
            throw new InvalidDataException(CATEGORY_ALREADY_CREATED + category.getNameCategory());
        }
        return true;
    }


    public boolean deleteCategoryById(@Positive(message = "id must be positive") Long id) {
        boolean res = categoryRepository.deleteCategoryById(id);
        isValid(res, CATEGORY_NOT_FOUND_WITH_ID + CATEGORY_HAVE_CHILD + id);
        return res;
    }

    private void isParentExists(Long id) {
        boolean parentExists = categoryRepository.checkExistsCategoryById(id);
        if (!parentExists) {
            throw new InvalidDataException(CATEGORY_PARENT_NOT_FOUND_WITH_ID + id);
        }
    }

    public boolean isCategoryExistsByName(String name) {
        return categoryRepository.checkExistsCategoryByName(name);
    }

    private void isCategoryExistsById(Long id) {
        boolean exists = categoryRepository.checkExistsCategoryById(id);
        if (!exists) {
            throw new InvalidDataException(CATEGORY_NOT_FOUND_WITH_ID + id);
        }
    }
}

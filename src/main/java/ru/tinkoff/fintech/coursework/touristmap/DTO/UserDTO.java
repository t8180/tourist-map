package ru.tinkoff.fintech.coursework.touristmap.DTO;

import lombok.ToString;
import lombok.Value;
import ru.tinkoff.fintech.coursework.touristmap.models.enums.RoleEnum;

import java.util.Objects;

@Value
@ToString
public class UserDTO {
    Long id;
    String username;
    RoleEnum role;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(id, userDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

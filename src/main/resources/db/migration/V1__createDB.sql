CREATE TABLE category
(
    id            SERIAL PRIMARY KEY,
    name_category VARCHAR(100) NOT NULL UNIQUE,
    parent_id     INTEGER,
    level         Integer      NOT NULL
);

CREATE TABLE sights
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(100)                     NOT NULL UNIQUE,
    coordinate  VARCHAR(250)                     NOT NULL,
    description VARCHAR(250),
    website     VARCHAR(50)                      NOT NULL,
    work_time   VARCHAR(250)                     NOT NULL,
    price       INTEGER                          NOT NULL,
    id_category INTEGER REFERENCES category (id) NOT NULL
);

insert into category (name_category, parent_id, level)
values ('all', null, 1);

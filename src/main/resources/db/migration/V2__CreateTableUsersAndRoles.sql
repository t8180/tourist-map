CREATE TYPE roles AS ENUM ('ROLE_ADMIN','ROLE_USER');

CREATE TABLE users
(
    id        SERIAL PRIMARY KEY,
    username  VARCHAR(30) NOT NULL UNIQUE,
    password  VARCHAR(80) NOT NULL,
    user_role roles       NOT NULL
);

CREATE CAST ( character varying as roles ) with inout AS ASSIGNMENT;
